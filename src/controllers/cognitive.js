const boom = require('boom')
const { faceVerification } = require('@root/src/mvp')

class CognitiveController {
  static async faceVerification (request) {
    try {
      return faceVerification(request.body)
    } catch (err) {
      throw boom.boomify(err)
    }
  }
}

module.exports = CognitiveController
