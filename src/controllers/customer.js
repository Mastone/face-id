const boom = require('boom')
const Customer = require('@models/customer')
const { faceRecognition } = require('@root/src/mvp')

class CustomerController {
  static async recognize (request) {
    try {
      const base64Image = request.body.image
      let customers = await Customer.find({}, 'id').populate('images')
      customers = customers.filter((customer) => customer.images && customer.images.length)

      const faceDescriptors = customers.map((customer) => {
        return {
          className: customer.id,
          faceDescriptors: customer.images.map((image) => image.faceDescriptors)
        }
      })

      const info = faceRecognition({ image: base64Image, faceDescriptors })

      if (info.success) {
        const data = await Customer.findById(info.customerId).populate({
          path: 'images',
          select: 'path',
          options: { limit: 1 }
        })
        return { data, info }
      }
      return { info }
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async create (request) {
    try {
      request.ability.throwUnlessCan('create', 'Customer')
      const info = { success: true, message: 'Клиент "{{customer}}" был создан.' }
      const customer = new Customer(request.body)
      const data = await customer.save()
      info.message = info.message.replace('{{customer}}', `${data.lastName} ${data.firstName} ${data.patronymic}`)

      return { data, info }
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async read (request) {
    try {
      return Customer.accessibleBy(request.ability).find().populate('images')
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async readById (request) {
    try {
      const id = request.params.id
      return Customer.accessibleBy(request.ability).findOne({ _id: id }).populate('images')
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async update (request) {
    try {
      const info = { success: true, message: 'Клиент "{{customer}}" был обновлен.' }
      const id = request.params.id
      const data = await Customer.accessibleBy(request.ability, 'update')
        .findOneAndUpdate({ _id: id }, request.body, { new: true }).populate('images')
      info.message = info.message.replace('{{customer}}', `${data.lastName} ${data.firstName} ${data.patronymic}`)

      return { data, info }
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async delete (request) {
    try {
      const info = { success: true, message: 'Клиент "{{customer}}" был удалён.' }
      const id = request.params.id
      const data = await Customer.accessibleBy(request.ability, 'delete').findOneAndRemove({ _id: id })
      info.message = info.message.replace('{{customer}}', `${data.lastName} ${data.firstName} ${data.patronymic}`)

      return { data, info }
    } catch (err) {
      throw boom.boomify(err)
    }
  }
}

module.exports = CustomerController
