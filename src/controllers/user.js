const boom = require('boom')
const bcrypt = require('bcryptjs')
const pick = require('lodash/pick')
const User = require('@models/user')
const defineAbilitiesFor = require('@abilities')

// Super User id 1 - может быть только 1 пользователь
// Сделать миграцию или что то другое, чтобы безболезненно разворачивать новый проект на любой тачке

class UserController {
  static async create (request) {
    try {
      // TODO: Зашифровать дефолтный пароль
      request.ability.throwUnlessCan('create', 'User')
      request.body.password = await bcrypt.hash('faceID2019', 10)
      const info = { success: true, message: 'Пользователь "{{user}}" был создан.' }
      const user = new User(request.body)
      const data = await user.save()
      info.message = info.message.replace('{{user}}', `${data.firstName} ${data.lastName}`)

      return { data, info }
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async read (request) {
    try {
      return User.accessibleBy(request.ability).find()
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async readById (request) {
    try {
      const id = request.params.id
      return User.accessibleBy(request.ability).findOne({ _id: id })
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async readCurrent (request) {
    try {
      const id = request.user._id
      const user = await User.findById(id).lean()
      user.rules = request.ability.rules
      return user
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async update (request) {
    try {
      const info = { success: true, message: 'Пользователь "{{user}}" был обновлен.' }
      const id = request.params.id
      const accessibleFields = User.accessibleFieldsBy(request.ability, 'update')
      const body = pick(request.body, accessibleFields)
      const updateOptions = { new: true, runValidators: true, context: 'query' }
      // TODO: Ненужный Доп. запрос
      const user = await User.findById(id)
      request.ability.throwUnlessCan('update', user)

      // Если пользователь обновляет себя, то отдаем правила на фронт
      if (request.user._id.toString() === id) {
        const data = await User.findOneAndUpdate({ _id: id }, body, updateOptions).lean()
        data.rules = defineAbilitiesFor(data).rules
        info.message = info.message.replace('{{user}}', `${data.firstName} ${data.lastName}`)

        return { data, info }
      } else {
        const data = await User.findOneAndUpdate({ _id: id }, body, updateOptions)
        info.message = info.message.replace('{{user}}', `${data.firstName} ${data.lastName}`)

        return { data, info }
      }
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async delete (request) {
    try {
      const info = { success: true, message: 'Пользователь "{{user}}" был удалён.' }
      const id = request.params.id
      const data = await User.accessibleBy(request.ability, 'delete').findOneAndRemove({ _id: id })
      info.message = info.message.replace('{{user}}', `${data.firstName} ${data.lastName}`)

      return { data, info }
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async changePassword (request) {
    try {
      const info = { success: true, message: 'Пароль для пользователя "{{user}}" был изменён.' }
      const user = await User.findById(request.params.id)
      request.ability.throwUnlessCan('update', user, 'password')
      const isMatch = request.user.role === 'SUPERUSER' || await bcrypt.compare(request.body.oldPassword, user.password)

      if (isMatch) {
        user.password = await bcrypt.hash(request.body.newPassword, 10)
        const data = await user.save()
        info.message = info.message.replace('{{user}}', `${data.firstName} ${data.lastName}`)

        return { data, info }
      } else {
        return Promise.reject(new Error('Старый пароль указан неверно'))
      }
    } catch (err) {
      throw boom.boomify(err)
    }
  }
}

module.exports = UserController
