const bcrypt = require('bcryptjs')
const User = require('@models/user')
const defineAbilitiesFor = require('@abilities')

class AuthController {
  static async verifyJWT (request, reply) {
    if (request.body && request.body.failureWithReply) { // И токен в черном списке
      reply.code(401).send({ 'error': 'Unauthorized' })
      return Promise.reject(new Error())
    }

    const token = request.headers.authorization.split(' ')[1]
    const decoded = await this.jwt.verify(token)

    if (!decoded || !decoded.login || !decoded.password) {
      return Promise.reject(new Error('Token not valid'))
    }

    const user = await User.findById(decoded._id)

    if (!user || decoded.login !== user.login || decoded.password !== user.password) {
      return Promise.reject(new Error('Token not valid'))
    }
    request.user = user
    request.ability = defineAbilitiesFor(user)
    // done()
  }
  static async login (request) {
    const reply = { userAuthorized: false }

    const conditions = {
      ...request.body.login && { login: request.body.login },
      ...request.body.email && { email: request.body.email },
      ...request.body.phone && { phone: request.body.phone }
    }

    const user = await User.findOne(conditions).lean()

    if (user) {
      const isMatch = await bcrypt.compare(request.body.password, user.password)

      if (isMatch) {
        const signUser = { _id: user._id, login: user.login, password: user.password }
        const token = await this.jwt.sign(signUser, { expiresIn: '2 days' })
        reply.userAuthorized = true
        reply.token = token
        reply.user = user
        reply.user.rules = defineAbilitiesFor(user).rules
      }
    }
    return reply
  }
  static async logout () {
    // const token = req.headers.authorization
    // jwt to black list
    return {}
  }
}

module.exports = AuthController
