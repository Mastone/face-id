const fs = require('fs')
const boom = require('boom')
const Image = require('@models/image')
const { trainNeuralNetwork } = require('@root/src/mvp')

class ImageController {
  static async create (request) {
    try {
      const base64Image = request.body.image
      const info = trainNeuralNetwork(base64Image)

      if (info.success) {
        const image = new Image()
        const base64ImageWithoutMeta = base64Image.replace(/^data:image\/\w+;base64,/, '')
        const extension = base64Image.substring('data:image/'.length, base64Image.indexOf(';base64'))
        const path = `public/uploads/${image._id.toString()}.${extension}`

        await fs.writeFile(path, base64ImageWithoutMeta, { encoding: 'base64' }, () => {})

        image.faceDescriptors = info.faceDescriptors
        image.customerId = request.params.id
        image.path = `/${path}`

        const data = await image.save()
        return { data, info }
      }
      return { info }
    } catch (err) {
      throw boom.boomify(err)
    }
  }

  static async delete (request) {
    try {
      const image = await Image.findById(request.params.id)
      const data = await image.remove()

      await fs.unlink(image.path.substring(1), () => {})
      return { data, info: { success: true, message: 'Фотография была удалена.' } }
    } catch (err) {
      throw boom.boomify(err)
    }
  }
}

module.exports = ImageController
