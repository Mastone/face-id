const UserController = require('@controllers/user')
const UserSchema = require('@schema/user')

const routes = [
  {
    method: 'POST',
    url: '/api/users',
    handler: UserController.create,
    schema: UserSchema.create
  },
  {
    method: 'GET',
    url: '/api/users',
    handler: UserController.read,
    schema: UserSchema.read
  },
  {
    method: 'GET',
    url: '/api/users/:id',
    handler: UserController.readById,
    schema: UserSchema.readById
  },
  {
    method: 'GET',
    url: '/api/users/current',
    handler: UserController.readCurrent,
    schema: UserSchema.readCurrent
  },
  {
    method: 'PUT',
    url: '/api/users/:id',
    handler: UserController.update,
    schema: UserSchema.update
  },
  {
    method: 'PUT',
    url: '/api/users/:id/password',
    handler: UserController.changePassword,
    schema: UserSchema.changePassword
  },
  {
    method: 'DELETE',
    url: '/api/users/:id',
    handler: UserController.delete,
    schema: UserSchema.delete
  }
]

module.exports = routes
