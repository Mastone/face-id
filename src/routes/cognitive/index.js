const CognitiveController = require('@controllers/cognitive')
const CognitiveSchema = require('@schema/cognitive')

const cognitiveRoutes = [
  {
    method: 'POST',
    url: '/api/cognitive-services/face-verification',
    handler: CognitiveController.faceVerification,
    schema: CognitiveSchema.faceVerification,
    bodyLimit: 1024 * 1024 * 6
  }
]

module.exports = cognitiveRoutes
