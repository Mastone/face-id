const AuthController = require('@controllers/auth')
const AuthSchema = require('@schema/auth')

const routes = [
  {
    method: 'POST',
    url: '/api/login',
    handler: AuthController.login,
    schema: AuthSchema.login,
    freeGate: true
  },
  {
    method: 'DELETE',
    url: '/api/logout',
    handler: AuthController.logout,
    schema: AuthSchema.logout
  }
]

module.exports = routes
