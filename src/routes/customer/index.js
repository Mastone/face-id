const CustomerController = require('@controllers/customer')
const CustomerSchema = require('@schema/customer')
const ImageController = require('@controllers/image')
const ImageSchema = require('@schema/image')

const routes = [
  {
    method: 'POST',
    url: '/api/customers',
    handler: CustomerController.create,
    schema: CustomerSchema.create
  },
  {
    method: 'POST',
    url: '/api/customers/:id/images',
    handler: ImageController.create,
    schema: ImageSchema.create,
    bodyLimit: 1024 * 1024 * 3
  },
  {
    method: 'GET',
    url: '/api/customers',
    handler: CustomerController.read,
    schema: CustomerSchema.read
  },
  {
    method: 'GET',
    url: '/api/customers/:id',
    handler: CustomerController.readById,
    schema: CustomerSchema.readById
  },
  {
    method: 'POST',
    url: '/api/customers/recognize',
    handler: CustomerController.recognize,
    schema: CustomerSchema.recognize,
    bodyLimit: 1024 * 1024 * 3
  },
  {
    method: 'PUT',
    url: '/api/customers/:id',
    handler: CustomerController.update,
    schema: CustomerSchema.update
  },
  {
    method: 'DELETE',
    url: '/api/customers/:id',
    handler: CustomerController.delete,
    schema: CustomerSchema.delete
  }
]

module.exports = routes
