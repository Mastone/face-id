const authRoutes = require('@routes/auth')
const userRoutes = require('@routes/user')
const imageRoutes = require('@routes/image')
const customerRoutes = require('@routes/customer')
const cognitiveRoutes = require('@routes/cognitive')

const routes = [
  ...authRoutes,
  ...userRoutes,
  ...imageRoutes,
  ...customerRoutes,
  ...cognitiveRoutes
]

module.exports = routes
