const ImageController = require('@controllers/image')
const ImageSchema = require('@schema/image')

const routes = [
  {
    method: 'DELETE',
    url: '/api/images/:id',
    handler: ImageController.delete,
    schema: ImageSchema.delete
  }
]

module.exports = routes
