class CustomerSchema {}

CustomerSchema.create = {
  description: 'Создать клиента может пользователь с ролью Менеджер, Администратор, Суперпользователь',
  tags: ['Клиенты'],
  summary: 'Создание нового клиента',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      firstName: { type: 'string', minLength: 1 },
      lastName: { type: 'string', minLength: 1 },
      patronymic: { type: 'string', minLength: 1 },
      companyName: { type: 'string', minLength: 1 },
      gender: { type: 'string', enum: ['MALE', 'FEMALE'] }
    },
    required: ['firstName', 'lastName', 'patronymic', 'companyName', 'gender']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            firstName: { type: 'string' },
            lastName: { type: 'string' },
            patronymic: { type: 'string' },
            companyName: { type: 'string' },
            gender: { type: 'string' },
            __v: { type: 'number' }
          }
        },
        info: {
          type: 'object',
          properties: {
            success: { type: 'boolean' },
            message: { type: 'string' }
          }
        }
      }
    }
  }
}

CustomerSchema.read = {
  description: 'Получение всех клиентов системы',
  tags: ['Клиенты'],
  summary: 'Получение клиентов',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'array',
      items: {
        type: 'object',
        properties: {
          _id: { type: 'string' },
          firstName: { type: 'string' },
          lastName: { type: 'string' },
          patronymic: { type: 'string' },
          companyName: { type: 'string' },
          gender: { type: 'string' },
          images: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                _id: { type: 'string' },
                customerId: { type: 'string' },
                path: { type: 'string' },
                __v: { type: 'number' }
              }
            }
          },
          __v: { type: 'number' }
        }
      }
    }
  }
}

CustomerSchema.readById = {
  description: 'Получение клиента по ID',
  tags: ['Клиенты'],
  summary: 'Получение клиента',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  params: {
    type: 'object',
    properties: {
      id: { type: 'string', description: 'Уникальный идентификатор клиента' }
    },
    required: ['id']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        _id: { type: 'string' },
        firstName: { type: 'string' },
        lastName: { type: 'string' },
        patronymic: { type: 'string' },
        companyName: { type: 'string' },
        gender: { type: 'string' },
        images: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              _id: { type: 'string' },
              customerId: { type: 'string' },
              path: { type: 'string' },
              __v: { type: 'number' }
            }
          }
        },
        __v: { type: 'number' }
      }
    }
  }
}

CustomerSchema.update = {
  description: 'Обновление клиента по ID',
  tags: ['Клиенты'],
  summary: 'Обновление клиента',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  params: {
    type: 'object',
    properties: {
      id: { type: 'string', description: 'Уникальный идентификатор клиента' }
    },
    required: ['id']
  },
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      firstName: { type: 'string', minLength: 1 },
      lastName: { type: 'string', minLength: 1 },
      patronymic: { type: 'string', minLength: 1 },
      companyName: { type: 'string', minLength: 1 },
      gender: { type: 'string', enum: ['MALE', 'FEMALE'] }
    }
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            firstName: { type: 'string' },
            lastName: { type: 'string' },
            patronymic: { type: 'string' },
            companyName: { type: 'string' },
            gender: { type: 'string' },
            images: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  _id: { type: 'string' },
                  customerId: { type: 'string' },
                  path: { type: 'string' },
                  __v: { type: 'number' }
                }
              }
            },
            __v: { type: 'number' }
          }
        },
        info: {
          type: 'object',
          properties: {
            success: { type: 'boolean' },
            message: { type: 'string' }
          }
        }
      }
    }
  }
}

CustomerSchema.delete = {
  description: 'Удаление клиента по ID',
  tags: ['Клиенты'],
  summary: 'Удаление клиента',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  params: {
    type: 'object',
    properties: {
      id: { type: 'string', description: 'Уникальный идентификатор клиента' }
    },
    required: ['id']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            __v: { type: 'number' }
          }
        },
        info: {
          type: 'object',
          properties: {
            success: { type: 'boolean' },
            message: { type: 'string' }
          }
        }
      }
    }
  }
}

CustomerSchema.recognize = {
  description: 'Распознавание клиента по фотографии',
  tags: ['Клиенты'],
  summary: 'Распознавание клиента',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  body: {
    type: 'object',
    properties: {
      image: { type: 'string' }
    },
    required: ['image']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            firstName: { type: 'string' },
            lastName: { type: 'string' },
            patronymic: { type: 'string' },
            companyName: { type: 'string' },
            gender: { type: 'string' },
            images: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  _id: { type: 'string' },
                  customerId: { type: 'string' },
                  path: { type: 'string' },
                  __v: { type: 'number' }
                }
              }
            },
            __v: { type: 'number' }
          }
        },
        info: {
          type: 'object',
          properties: {
            success: { type: 'boolean' },
            message: { type: 'string' }
          }
        }
      }
    }
  }
}

module.exports = CustomerSchema
