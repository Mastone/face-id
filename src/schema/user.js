class UserSchema {}

UserSchema.create = {
  description: 'Создать пользователя может пользователь с ролью Суперпользователь, Администратор',
  tags: ['Пользователи'],
  summary: 'Создание нового пользователя',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  body: {
    type: 'object',
    properties: {
      firstName: { type: 'string', minLength: 1 },
      lastName: { type: 'string', minLength: 1 },
      patronymic: { type: 'string' },
      login: { type: 'string', minLength: 1 },
      email: { type: 'string', minLength: 1 },
      phone: { type: 'string' },
      avatar: { type: 'string' },
      role: { type: 'string', enum: ['USER', 'MANAGER', 'ADMIN', 'SUPERUSER'] }
    },
    required: ['firstName', 'lastName', 'login', 'email', 'role']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            firstName: { type: 'string' },
            lastName: { type: 'string' },
            patronymic: { type: 'string' },
            login: { type: 'string' },
            email: { type: 'string' },
            phone: { type: 'string' },
            avatar: { type: 'string' },
            role: { type: 'string' },
            __v: { type: 'number' }
          }
        },
        info: {
          type: 'object',
          properties: {
            success: { type: 'boolean' },
            message: { type: 'string' }
          }
        }
      }
    }
  }
}

UserSchema.read = {
  description: 'Получение всех пользователей системы',
  tags: ['Пользователи'],
  summary: 'Получение пользователей',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'array',
      items: {
        type: 'object',
        properties: {
          _id: { type: 'string' },
          firstName: { type: 'string' },
          lastName: { type: 'string' },
          patronymic: { type: 'string' },
          login: { type: 'string' },
          email: { type: 'string' },
          phone: { type: 'string' },
          avatar: { type: 'string' },
          role: { type: 'string' },
          __v: { type: 'number' }
        }
      }
    }
  }
}

UserSchema.readById = {
  description: 'Получение пользователя по ID',
  tags: ['Пользователи'],
  summary: 'Получение пользователя',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  params: {
    type: 'object',
    properties: {
      id: { type: 'string', description: 'Уникальный идентификатор пользователя' }
    },
    required: ['id']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        _id: { type: 'string' },
        firstName: { type: 'string' },
        lastName: { type: 'string' },
        patronymic: { type: 'string' },
        login: { type: 'string' },
        email: { type: 'string' },
        phone: { type: 'string' },
        avatar: { type: 'string' },
        role: { type: 'string' },
        __v: { type: 'number' }
      }
    }
  }
}

UserSchema.readCurrent = {
  description: 'Получение текущего авторизованного пользователя системы',
  tags: ['Пользователи'],
  summary: 'Получение текущего пользователя',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        _id: { type: 'string' },
        firstName: { type: 'string' },
        lastName: { type: 'string' },
        patronymic: { type: 'string' },
        login: { type: 'string' },
        email: { type: 'string' },
        phone: { type: 'string' },
        avatar: { type: 'string' },
        role: { type: 'string' },
        rules: {
          type: 'array',
          items: { additionalProperties: true, type: 'object' }
        },
        __v: { type: 'number' }
      }
    }
  }
}

UserSchema.update = {
  description: 'Обновление пользователя по ID',
  tags: ['Пользователи'],
  summary: 'Обновление пользователя',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  params: {
    type: 'object',
    properties: {
      id: { type: 'string', description: 'Уникальный идентификатор пользователя' }
    },
    required: ['id']
  },
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      firstName: { type: 'string', minLength: 1 },
      lastName: { type: 'string', minLength: 1 },
      patronymic: { type: 'string' },
      login: { type: 'string', minLength: 1 },
      email: { type: 'string', minLength: 1 },
      phone: { type: 'string' },
      avatar: { type: 'string' },
      role: { type: 'string', enum: ['USER', 'MANAGER', 'ADMIN', 'SUPERUSER'] }
    }
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            firstName: { type: 'string' },
            lastName: { type: 'string' },
            patronymic: { type: 'string' },
            login: { type: 'string' },
            email: { type: 'string' },
            phone: { type: 'string' },
            avatar: { type: 'string' },
            role: { type: 'string' },
            rules: {
              type: 'array',
              items: { additionalProperties: true, type: 'object' }
            },
            __v: { type: 'number' }
          }
        },
        info: {
          type: 'object',
          properties: {
            success: { type: 'boolean' },
            message: { type: 'string' }
          }
        }
      }
    }
  }
}

UserSchema.delete = {
  description: 'Удаление пользователя по ID',
  tags: ['Пользователи'],
  summary: 'Удаление пользователя',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  params: {
    type: 'object',
    properties: {
      id: { type: 'string', description: 'Уникальный идентификатор пользователя' }
    },
    required: ['id']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            __v: { type: 'number' }
          }
        },
        info: {
          type: 'object',
          properties: {
            success: { type: 'boolean' },
            message: { type: 'string' }
          }
        }
      }
    }
  }
}

UserSchema.changePassword = {
  description: 'Обновление пароля пользователя по ID',
  tags: ['Пользователи'],
  summary: 'Обновление пароля пользователя',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  params: {
    type: 'object',
    properties: {
      id: { type: 'string', description: 'Уникальный идентификатор пользователя' }
    },
    required: ['id']
  },
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      oldPassword: { type: 'string' },
      newPassword: { type: 'string', minLength: 1 }
    },
    required: ['oldPassword', 'newPassword']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            __v: { type: 'number' }
          }
        },
        info: {
          type: 'object',
          properties: {
            success: { type: 'boolean' },
            message: { type: 'string' }
          }
        }
      }
    }
  }
}

module.exports = UserSchema
