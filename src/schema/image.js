class ImageSchema {}

ImageSchema.create = {
  description: 'Создать изображение клиента',
  tags: ['Клиенты'],
  summary: 'Создание нового изображения клиента',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  params: {
    type: 'object',
    properties: {
      id: { type: 'string', description: 'Уникальный идентификатор клиента' }
    },
    required: ['id']
  },
  body: {
    type: 'object',
    properties: {
      image: { type: 'string' }
    },
    required: ['image']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            customerId: { type: 'string' },
            path: { type: 'string' },
            __v: { type: 'number' }
          }
        },
        info: {
          type: 'object',
          properties: {
            success: { type: 'boolean' },
            message: { type: 'string' }
          }
        }
      }
    }
  }
}

ImageSchema.delete = {
  description: 'Удаление изображения клиента по ID изображения',
  tags: ['Клиенты'],
  summary: 'Удаление изображения клиента',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  params: {
    type: 'object',
    properties: {
      id: { type: 'string', description: 'Уникальный идентификатор изображения клиента' }
    },
    required: ['id']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            customerId: { type: 'string' },
            path: { type: 'string' },
            __v: { type: 'number' }
          }
        },
        info: {
          type: 'object',
          properties: {
            success: { type: 'boolean' },
            message: { type: 'string' }
          }
        }
      }
    }
  }
}

module.exports = ImageSchema
