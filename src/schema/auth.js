class AuthSchema {}

AuthSchema.login = {
  description: 'Вход пользователя в систему',
  tags: ['Авторизация'],
  summary: 'Авторизация',
  body: {
    type: 'object',
    properties: {
      login: { type: 'string', minLength: 1 },
      email: { type: 'string', minLength: 1 },
      phone: { type: 'string', minLength: 1 },
      password: { type: 'string', minLength: 1 }
    },
    required: ['password'],
    oneOf: [
      { required: ['login'] },
      { required: ['email'] },
      { required: ['phone'] }
    ]
  },
  response: {
    200: {
      description: 'Пользователь авторизован',
      type: 'object',
      properties: {
        userAuthorized: { type: 'boolean' },
        token: { type: 'string' },
        user: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            firstName: { type: 'string' },
            lastName: { type: 'string' },
            patronymic: { type: 'string' },
            login: { type: 'string' },
            email: { type: 'string' },
            phone: { type: 'string' },
            avatar: { type: 'string' },
            role: { type: 'string' },
            rules: {
              type: 'array',
              items: { additionalProperties: true, type: 'object' }
            },
            __v: { type: 'number' }
          }
        }
      }
    }
  }
}

AuthSchema.logout = {
  description: 'Выход пользователя из системы',
  tags: ['Авторизация'],
  summary: 'Деавторизация',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  response: {
    200: {
      description: 'Пользователь деавторизован',
      type: 'object',
      properties: {}
    }
  }
}

module.exports = AuthSchema
