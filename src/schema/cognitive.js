class CognitiveSchema {}

CognitiveSchema.faceVerification = {
  description: 'API поможет оценить вероятность того, что на двух разных изображениях представлено лицо одного человека',
  tags: ['Распознавание лиц'],
  summary: 'Определение вероятности того, что на двух разных изображениях представлено лицо одного человека',
  headers: {
    type: 'object',
    properties: {
      'authorization': { type: 'string', description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6I...' }
    },
    required: ['authorization']
  },
  body: {
    type: 'object',
    properties: {
      firstImage: { type: 'string' },
      secondImage: { type: 'string' }
    },
    required: ['firstImage', 'secondImage']
  },
  response: {
    200: {
      description: 'Успешный ответ',
      type: 'object',
      properties: {
        success: { type: 'boolean' },
        message: { type: 'string' },
        distance: { type: 'number' },
        firstRect: {
          type: 'object',
          properties: {
            top: { type: 'number' },
            left: { type: 'number' },
            width: { type: 'number' },
            height: { type: 'number' }
          }
        },
        secondRect: {
          type: 'object',
          properties: {
            top: { type: 'number' },
            left: { type: 'number' },
            width: { type: 'number' },
            height: { type: 'number' }
          }
        }
      }
    }
  }
}

module.exports = CognitiveSchema
