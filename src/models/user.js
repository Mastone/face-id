const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const { accessibleRecordsPlugin, accessibleFieldsPlugin } = require('@casl/mongoose')

// TODO: Не хранить аватар в строке, реализовать хранение изображения, как у клиентов
const schema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  patronymic: String,
  login: {
    type: String,
    required: true,
    index: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    index: true,
    unique: true,
    uniqueCaseInsensitive: true,
    lowercase: true
  },
  phone: {
    type: String,
    unique: true,
    sparse: true
  },
  password: String,
  avatar: String,
  role: {
    type: String,
    required: true,
    enum: ['USER', 'MANAGER', 'ADMIN', 'SUPERUSER'],
    default: 'USER'
  }
})

schema.plugin(uniqueValidator)
schema.plugin(accessibleRecordsPlugin)
schema.plugin(accessibleFieldsPlugin)

module.exports = mongoose.model('User', schema)
