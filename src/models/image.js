const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  customerId: {
    type: mongoose.Schema.ObjectId,
    required: true
  },
  path: {
    type: String,
    required: true
  },
  faceDescriptors: {
    type: [Number],
    required: true
  }
})

module.exports = mongoose.model('Image', schema)
