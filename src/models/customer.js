const mongoose = require('mongoose')
const { accessibleRecordsPlugin } = require('@casl/mongoose')

const schema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  patronymic: {
    type: String,
    required: true
  },
  companyName: {
    type: String,
    required: true
  },
  gender: {
    type: String,
    required: true,
    enum: ['MALE', 'FEMALE'],
    default: 'MALE'
  }
},
{
  toJSON: { virtuals: true },
  toObject: { virtuals: true }
})

schema.virtual('images', {
  ref: 'Image',
  localField: '_id',
  foreignField: 'customerId'
})

schema.plugin(accessibleRecordsPlugin)

module.exports = mongoose.model('Customer', schema)
