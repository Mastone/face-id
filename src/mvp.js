const cv = require('opencv4nodejs')
const fr = require('face-recognition').withCv(cv)

const detector = fr.FaceDetector()
const recognizer = fr.FaceRecognizer()
const UNKNOWN_THRESHOLD = 0.6

const decodeFromBase64 = (base64String) => {
  base64String = base64String.replace(/^data:image\/\w+;base64,/, '')
  const buffer = Buffer.from(base64String, 'base64')
  const imageCvMat = cv.imdecode(buffer)
  const imageCv = fr.CvImage(imageCvMat)
  return fr.cvImageToImageRGB(imageCv)
}

const round = (num, precision = 2) => {
  const f = Math.pow(10, precision)
  return Math.round(num * f) / f
}

const prepareRect = ({ width, height }, rect) => {
  const result = {}
  result.top = round(rect.top / height * 100)
  result.left = round(rect.left / width * 100)
  result.width = round((rect.right - rect.left) / width * 100)
  result.height = round((rect.bottom - rect.top) / height * 100)
  return result
}

const faceVerification = ({ firstImage, secondImage }) => {
  const result = {
    success: false,
    message: 'Не удалось верифицировать изображения.',
    firstRect: {},
    secondRect: {}
  }

  const firstImageRGB = decodeFromBase64(firstImage)
  const secondImageRGB = decodeFromBase64(secondImage)

  if (firstImageRGB && secondImageRGB) {
    const firstFaceImages = detector.detectFaces(firstImageRGB)
    const secondFaceImages = detector.detectFaces(secondImageRGB)

    if (firstFaceImages[0] && secondFaceImages[0]) {
      const firstFaceRects = detector.locateFaces(firstImageRGB)
      const secondFaceRects = detector.locateFaces(secondImageRGB)

      const firstFaceDescriptors = new fr.Array(recognizer.getFaceDescriptors(firstFaceImages[0]))
      const secondFaceDescriptors = new fr.Array(recognizer.getFaceDescriptors(secondFaceImages[0]))
      const distance = round(fr.distance(firstFaceDescriptors, secondFaceDescriptors))

      if (distance < UNKNOWN_THRESHOLD) {
        result.message = 'Оба изображения принадлежат одному человеку.'
        result.success = true
      } else {
        result.message = 'Оба изображения принадлежат разным людям.'
      }
      result.distance = distance
      result.firstRect = prepareRect({ width: firstImageRGB.cols, height: firstImageRGB.rows }, firstFaceRects[0].rect)
      result.secondRect = prepareRect({ width: secondImageRGB.cols, height: secondImageRGB.rows }, secondFaceRects[0].rect)
    } else {
      result.message = 'Лица не определены на одном или нескольких из указанных изображений.'
    }
  } else {
    result.message = 'Невозможно декодировать одно или несколько из указанных изображений.'
  }
  return result
}

const faceRecognition = ({ image, faceDescriptors }) => {
  const result = {
    success: false,
    message: 'База изображений клиентов пуста. Распознавание лиц недоступно.'
  }
  recognizer.load(faceDescriptors)

  // Empty model
  if (recognizer.getDescriptorState().length) {
    const imageRGB = decodeFromBase64(image)

    if (imageRGB) {
      const faceImages = detector.detectFaces(imageRGB)

      if (faceImages[0]) {
        const bestPrediction = recognizer.predictBest(faceImages[0], UNKNOWN_THRESHOLD)
        const className = bestPrediction.className

        if (className !== 'unknown') {
          result.success = true
          result.customerId = className
          result.message = `Лицо распознано.`
        } else {
          result.message = `
            Лицо обнаружено, но не распознано.
            Возможно, изображение клиента отсутствует в системе.
          `
        }
      } else {
        result.message = `
          Лицо не обнаружено.
          Возможно, следует улучшить освещение изображения или выбрать другое.
        `
      }
    } else {
      result.message = 'Невозможно декодировать изображение.'
    }
  }
  return result
}

const trainNeuralNetwork = (image) => {
  const result = {
    success: false,
    faceDescriptors: [],
    message: 'Не удалось обучить нейронную сеть.'
  }

  const imageRGB = decodeFromBase64(image)

  if (imageRGB) {
    const faceImages = detector.detectFaces(imageRGB)

    if (faceImages[0]) {
      result.faceDescriptors = recognizer.getFaceDescriptors(faceImages[0])
      result.message = 'Фотография была загружена.'
      result.success = true
    } else {
      result.message = `
        Лицо не обнаружено.
        Возможно, следует улучшить освещение изображения или выбрать другое.
      `
    }
  } else {
    result.message = 'Невозможно декодировать изображение.'
  }
  return result
}

module.exports = {
  faceVerification,
  faceRecognition,
  trainNeuralNetwork
}
