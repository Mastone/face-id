const { AbilityBuilder } = require('@casl/ability')

const defineAbilitiesFor = (user) => {
  return AbilityBuilder.define((can, cannot) => {
    if (user.role === 'USER' || user.role === 'MANAGER') {
      can(['read', 'delete'], 'User', { _id: user._id })
      can(
        'update',
        'User',
        ['firstName', 'lastName', 'patronymic', 'login', 'email', 'phone', 'password', 'avatar'],
        { _id: user._id }
      )
    }

    (user.role === 'MANAGER' || user.role === 'ADMIN') && can('manage', 'Customer')

    if (user.role === 'ADMIN') {
      can('read', 'Users')
      can(['create', 'read'], 'User')
      can(['update', 'delete'], 'User', { role: { $ne: 'SUPERUSER' } })
      cannot('update', 'User', 'password', { _id: { $ne: user._id } })
    } else if (user.role === 'SUPERUSER') {
      can('manage', 'all')
      cannot('delete', 'User', { role: 'SUPERUSER' })
    }
  })
}

module.exports = defineAbilitiesFor
