const path = require('path')
const mongoose = require('mongoose')
const fastify = require('fastify')({ logger: true })

const routes = require('@routes')
const swaggerConfig = require('@config/swagger')
const AuthController = require('@controllers/auth')

const applyRoutes = () => {
  routes.forEach((route) => {
    if (!route.freeGate) {
      route.beforeHandler = fastify.auth([fastify.verifyJWT])
    }
    fastify.route(route)
  })
}

fastify.decorate('verifyJWT', AuthController.verifyJWT)
fastify.decorate('login', AuthController.login)
fastify.decorateRequest('user', undefined)
fastify.decorateRequest('ability', undefined)

fastify
  .register(require('fastify-swagger'), swaggerConfig.options)
  .register(require('fastify-jwt'), { secret: 'supersecret' })
  .register(require('fastify-auth'))
  .after(applyRoutes)

fastify.register(require('fastify-static'), {
  root: path.join(__dirname, '../public'),
  prefix: '/public/'
})

mongoose.connect('mongodb://localhost/mycargarage', {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
}).catch(err => console.log(err))

const bootstrap = async () => {
  try {
    await fastify.listen(3000)
    fastify.swagger()
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}

bootstrap()
