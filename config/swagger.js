exports.options = {
  routePrefix: '/documentation',
  exposeRoute: true,
  swagger: {
    info: {
      title: 'Face ID',
      description: 'Веб-приложение по распознаванию лиц',
      version: '0.0.0'
    },
    externalDocs: {
      url: '/',
      description: 'Production'
    },
    host: 'localhost',
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json']
  }
}
